const BASE_URL = "https://promotron-counterparty-integration.samokat.io/counterparty";

export const fetchCuounterparties = (search:null|string) =>
{
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        offset: 0,
        filter: {
          nameOrGuid: search,
          isSupplier: true,
        },
      }),
    };
    return fetch(`${BASE_URL}/find-short-by-filters`, requestOptions)
      .then((response) => response.json());
}

export const fetchCuounterpartyInfo = (id:string)=>
{
   return fetch(`${BASE_URL}/${id}`)
      .then((response) => response.json())
}