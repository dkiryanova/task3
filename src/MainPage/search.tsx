import React from "react";
import { Navigate, useNavigate } from "react-router-dom";
import "./search.css";

interface State {
  search: string;
}

interface Props {
  enterHandler: (search: string) => void;
}

export class Search extends React.Component<Props, State> {
  state = {
    search: "",
  };
  
  handleEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      this.props.enterHandler(this.state.search);
    }
  };

  render() {
    return (
      <div className="row_search">
        <input
          type="text"
          value={this.state.search}
          onChange={(event) => this.setState({ search: event.target.value })}
          onKeyUp={this.handleEnter}
          placeholder="Поиск"
        />
        <button onClick={() => this.props.enterHandler(this.state.search)}>
          Найти
        </button>
      </div>
    );
  }
}
