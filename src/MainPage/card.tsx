import "./card.css";
import React from "react";
import { Counterparty } from "./main";
import { Link, Navigate } from "react-router-dom";

interface Props {
  counterparties: Counterparty[];
}

export class Card extends React.Component<Props, {}> {
  render() {
    return (
      <div className="cards">
        {this.props.counterparties.map((item) => (
          <div key={item.id} className="card_counterparty">
            <Link to={`/suppliers/${item.id}`}><a>{item.name}</a></Link>
          </div>
        ))}
      </div>
    );
  }
}
