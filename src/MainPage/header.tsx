import React from "react";
import "./header.css";
import { Search } from "./search";

interface Props {
  enterHandler: (search: string) => void;
}

export class Header extends React.Component<Props, {}> {
  handleEnter = (search: string) => {
    this.props.enterHandler(search);
  };

  render() {
    return (
      <header>
        <div className="name_sheet"> Поставщики</div>
        <Search enterHandler={this.handleEnter} />
      </header>
    );
  }
}
