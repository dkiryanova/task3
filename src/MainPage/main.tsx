import React from "react";
import { useLocation } from "react-router-dom";
import { Card } from "./card";
import { Search } from "./search";
import {fetchCuounterparties} from "../api/CuounterpartyAPI"

interface Props {
  search: string;
}

export interface Counterparty {
  id: string;
  name: string;
}

interface State {
  counterparties: Counterparty[];
}

export class Main extends React.Component<Props, State> {
  state: State = {
    counterparties: [],
  };

  componentDidMount() {
    const search = new URLSearchParams(this.props.search).get("search");
    fetchCuounterparties(search).then((data) => this.setState({ counterparties: data.value.data }));
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    const search = new URLSearchParams(this.props.search).get("search");
    if (prevProps.search !== this.props.search) {
      fetchCuounterparties(search).then((data) => this.setState({ counterparties: data.value.data }));
    }
  }

  render() {
    return <Card counterparties={this.state.counterparties} />;
  }
}
