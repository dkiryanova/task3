import "../App.css";
import { Header } from "./header";
import { Main } from "./main";
import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";

function MainPage() {
  const [search, setSearch] = useState("");
  const navigate = useNavigate();

  const handleEnter = (search: string) => {
    setSearch(search);
    if(search != "")
    {
      navigate(`/suppliers?search=${search}`);
    }
    else{
      navigate(`/suppliers`);
    }
  };

  return (
    <div>
      <Header enterHandler={handleEnter} />
      <Main search={useLocation().search} />
    </div>
  );
}

export default MainPage;
