import React from "react";
import { MainInfo } from "./SupplierInfoPage/mainInfo";
import { render, waitFor, screen } from "@testing-library/react";
import { fetchCuounterpartyInfo } from "./api/CuounterpartyAPI";

const fakeCuounterpartyInfo = {
  value: {
    name: "Оганян Тигран Юрьевич ИП",
    deletionMark: true,
    inn: "231113642598",
    contractorType: "Физическое лицо",
    ediProvider: "",
    factAddress: "Санкт-Петербург г., Ланское ш., д. 14, корп. 1, лит. А",
    contactInfo: [
      { value: { value: "89615160000" } },
      { value: { value: "solnechnaya.25@mail.ru" } },
    ],
  },
};

const fakeCuounterpartyInfo_phoneNumber = {
  value: {
    name: "Давиденко Дмитрий Вадимович",
    deletionMark: false,
    inn: "510105192081",
    contractorType: "Физическое лицо",
    ediProvider: "МАСЛОЗАВОД НЫТВЕНСКИЙ ООО",
    factAddress: "Москва г., Главмосстроя ул., д. 20",
    contactInfo: [{ value: { value: "89641040699" } }],
  },
};

const fakeCuounterpartyInfo_email = {
  value: {
    name: "Павлов Андрей Алексеевич",
    deletionMark: false,
    inn: "210701671288",
    contractorType: "Физическое лицо",
    ediProvider: "",
    factAddress: "Москва г., Главмосстроя ул., д. 20",
    contactInfo: [
      { value: { value: "" } },
      { value: { value: "spb.bryanceva13@samokat-team.ru" } },
    ],
  },
};

const fakeCuounterpartyInfo_empty = {
  value: {
    name: "Нам Денис Аркадьевич",
    deletionMark: false,
    inn: "781902388802",
    contractorType: "Физическое лицо",
    ediProvider: "",
    factAddress: "",
    contactInfo: [],
  },
};

jest.mock("./api/CuounterpartyAPI");

describe("MainInfo component", () => {
  test("it renders phoneNumber and email", async () => {
    fetchCuounterpartyInfo.mockResolvedValueOnce(fakeCuounterpartyInfo);
    const component = render(<MainInfo />);
    await waitFor(() => {
      expect(
        screen.getByText("89615160000", { selector: "#phoneNumber_value" })
      ).toBeInTheDocument();
      expect(
        screen.getByText("solnechnaya.25@mail.ru", { selector: "#email_value" })
      ).toBeInTheDocument();
    });
  });

  test("it renders phoneNumber", async () => {
    fetchCuounterpartyInfo.mockResolvedValueOnce(
      fakeCuounterpartyInfo_phoneNumber
    );
    const component = render(<MainInfo />);
    await waitFor(() => {
      expect(
        screen.getByText("89641040699", { selector: "#phoneNumber_value" })
      ).toBeInTheDocument();
      expect(
        screen.getByText("", { selector: "#email_value" })
      ).toBeInTheDocument();
    });
  });

  test("it renders email", async () => {
    fetchCuounterpartyInfo.mockResolvedValueOnce(fakeCuounterpartyInfo_email);
    const component = render(<MainInfo />);
    await waitFor(() => {
      expect(
        screen.getByText("", { selector: "#phoneNumber_value" })
      ).toBeInTheDocument();
      expect(
        screen.getByText("spb.bryanceva13@samokat-team.ru", {
          selector: "#email_value",
        })
      ).toBeInTheDocument();
    });
  });

  test("it renders phoneNumber and email empty", async () => {
    fetchCuounterpartyInfo.mockResolvedValueOnce(fakeCuounterpartyInfo_empty);
    const component = render(<MainInfo />);
    await waitFor(() => {
      expect(
        screen.getByText("", { selector: "#phoneNumber_value" })
      ).toBeInTheDocument();
      expect(
        screen.getByText("", { selector: "#email_value" })
      ).toBeInTheDocument();
    });
  });

  test("it renders status_delete", async () => {
    fetchCuounterpartyInfo.mockResolvedValueOnce(fakeCuounterpartyInfo);
    const component = render(<MainInfo />);
    await waitFor(() => {
      expect(screen.getByText(/УДАЛЕН/i)).toBeInTheDocument();
    });
  });

  test("it renders status_edi", async () => {
    fetchCuounterpartyInfo.mockResolvedValueOnce(
      fakeCuounterpartyInfo_phoneNumber
    );
    const component = render(<MainInfo />);
    await waitFor(() => {
      expect(screen.getByText(/EDI/i)).toBeInTheDocument();
    });
  });
});
