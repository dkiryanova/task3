import React from "react";
import { CardInfo } from "./cardInfo";
import { fetchCuounterpartyInfo } from "../api/CuounterpartyAPI";

interface Props {
  guid: any;
}

export interface State {
  name: string;
  inn: string;
  contractorType: string;
  phoneNumber: string;
  email: string;
  factAddress: string;
  deletionMark: boolean;
  ediProvider: string;
}

export class MainInfo extends React.Component<Props, State> {
  state: State = {
    name: "",
    inn: "",
    contractorType: "",
    phoneNumber: "",
    email: "",
    factAddress: "",
    deletionMark: false,
    ediProvider: "",
  };

  componentDidMount() {
    fetchCuounterpartyInfo(this.props.guid).then((data) =>
     this.setState({
            ...data.value,
            phoneNumber: data.value.contactInfo?.[0]?.value?.value,
            email: data.value.contactInfo?.[1]?.value?.value,
          })
    );
  }

  render() {
    return <CardInfo {...this.state} />;
  }
}
