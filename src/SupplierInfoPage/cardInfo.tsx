import React from "react";
import "./cardInfo.css";
import { State as Props } from "./mainInfo";

export class CardInfo extends React.Component<Props> {
  render() {
    return (
      <div className="cardInfo">
        <div className="header">
          <div className="name">{this.props.name}</div>
          <div className="status">
            {this.props.deletionMark ? (
              <div className="status_delete">УДАЛЕН</div>
            ) : (
              <div></div>
            )}
            {this.props.ediProvider === "" ? (
              <div></div>
            ) : (
              <div className="status_edi">EDI</div>
            )}
          </div>
        </div>
        <div className="inn">
          <div className="parameter">ИНН</div>
          <div className="value">{this.props.inn}</div>
        </div>
        <div className="contractorType">
          <div className="parameter">ТИП</div>
          <div className="value">{this.props.contractorType}</div>
        </div>
        <div className="phoneNumber">
          <div className="parameter">ТЕЛЕФОН</div>
          <div id="phoneNumber_value" className="value">{this.props.phoneNumber}</div>
        </div>
        <div className="email">
          <div className="parameter">EMAIL</div>
          <div id="email_value" className="value">{this.props.email}</div>
        </div>
        <div className="factAddress">
          <div className="parameter">ЮР.АДРЕС</div>
          <div className="value">{this.props.factAddress}</div>
        </div>
      </div>
    );
  }
}
