import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import { HeaderBack } from "./headerBack";
import { MainInfo } from "./mainInfo";


function SupplierInfoPage (){
    return (
      <div>
        <HeaderBack history={useNavigate()}/>
        <MainInfo guid={useParams().guid}/>
      </div>
    );
}

export default SupplierInfoPage