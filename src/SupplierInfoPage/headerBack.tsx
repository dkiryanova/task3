import React from "react";
import { Link } from "react-router-dom";
import "../MainPage/header.css";

interface Props {
  history: any;
}

export class HeaderBack extends React.Component<Props, {}> {
  render() {
    return (
      <header>
        <a className="link_back" onClick={() => this.props.history(-1)}>&lsaquo; назад</a>
      </header>
    );
  }
}
