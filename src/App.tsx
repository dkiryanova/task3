import "./App.css";
import  MainPage  from "./MainPage/MainPage";
import React from "react";
import  SupplierInfoPage  from "./SupplierInfoPage/SupplierInfoPage";
import { Routes, Route, Navigate } from "react-router-dom";

export class App extends React.Component {
  render() {
    return (
        <Routes>
          <Route path="/" element={<Navigate replace to="/suppliers" />} />
          <Route path="/suppliers" element={<MainPage />} />
          <Route path="/suppliers/:guid" element={<SupplierInfoPage />} />
          <Route
            path="*"
            element={
              <main style={{ textAlign: "center" }}>
                <p>Страница не найдена</p>
              </main>
            }
          />
        </Routes>
    );
  }
}
